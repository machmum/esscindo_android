package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/21/2016.
 */
public class OpenForm {
    private String id;
    private Field[] fields;
    private Rule[] rules;

    public OpenForm(String id, Field[] fields, Rule[] rules) {
        this.id = id;
        this.fields = fields;
        this.rules = rules;
    }

    public OpenForm() {
        this.id = "";
        this.fields = null;
        this.rules = null;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFields(Field[] fields) {
        this.fields = fields;
    }

    public void setRules(Rule[] rules) {
        this.rules = rules;
    }

    public String getId() {
        return id;
    }

    public Field[] getFields() {
        return fields;
    }

    public Rule[] getRules() {
        return rules;
    }
}

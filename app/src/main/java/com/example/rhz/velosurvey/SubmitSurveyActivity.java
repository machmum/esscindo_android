package com.example.rhz.velosurvey;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.rhz.velosurvey.ApiData.ApiAddress;
import com.example.rhz.velosurvey.DataLocal.DBHandler;
import com.example.rhz.velosurvey.DataLocal.SavedSurvey;
import com.example.rhz.velosurvey.FormData.AdapterSubmit;
import com.example.rhz.velosurvey.UserData.User;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubmitSurveyActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();

    ListView list_survey;
    SavedSurvey[] listSurvey;
    DBHandler db;
    public ApiAddress apiAddress;
    UserLocalStore userLocalStore;
    User user;
    AdapterSubmit adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumbit_survey);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();
        db = new DBHandler(this);
        Button submitButton = (Button) findViewById(R.id.submitButtonSubmit);
        Button selectAllButton = (Button) findViewById(R.id.submitButtonSelectAll);
        Button disselectAllButton = (Button) findViewById(R.id.buttonSubmitDisselect);
        disselectAllButton.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        selectAllButton.setOnClickListener(this);
        getDataFromDB();
        listSurvey();


        //getDataFromDB();
        //Toast.makeText(SubmitSurveyActivity.this, listSurvey[0].getIdForm(), Toast.LENGTH_LONG).show();


    }

    public void getDataFromDB() {
        List<SavedSurvey> surveys = db.getAllSurvey(user.userId);
        listSurvey = new SavedSurvey[surveys.size()];
        int n = 0;
        for (SavedSurvey survey : surveys) {
            String formId = survey.getIdForm();
            String formName = db.getFormNameById(formId);
            listSurvey[n++] = new SavedSurvey(survey.getId(), formId, formName, survey.getData(), survey.getSubmited());
        }
    }

    public void listSurvey() {
        list_survey = (ListView) findViewById(R.id.savedList);

        adapter = new AdapterSubmit(SubmitSurveyActivity.this, listSurvey);

        list_survey.setAdapter(adapter);


        list_survey.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SavedSurvey survey = (SavedSurvey) list_survey.getItemAtPosition(position);

                        Toast.makeText(SubmitSurveyActivity.this, survey.getData(), Toast.LENGTH_LONG).show();

                        //startNewForm(form);
                    }
                }
        );

        //int n = list_survey.getCount();

    }

    public void submitSurvey() {
        int x = 0;
        //Toast.makeText(SubmitSurveyActivity.this, "Coba di sini", Toast.LENGTH_SHORT).show();
        /*
        for(int n=0;n<list_survey.getCount();n++) {
            View v = list_survey.getChildAt(n);
            CheckBox chBox = (CheckBox) v.findViewById(R.id.checkBoxListSurvey);
            SavedSurvey survey = (SavedSurvey) list_survey.getItemAtPosition(n);
            //x += chBox.isChecked() ? 1 : 0;
            if (chBox.isChecked() && chBox.isClickable()) {
                submitToServer(survey);
                chBox.setClickable(false);
                //chBox.setActivated(false);
              x++;
            }
        }
        */
        for (int n = 0; n < listSurvey.length; n++) {
            SavedSurvey survey = listSurvey[n];
            if (1 == survey.getCeklis()) {
                submitToServer(survey);
                //chBox.setClickable(false);
                //chBox.setActivated(false);
                x++;
            }
        }

        if (0 == x) {
            Toast.makeText(SubmitSurveyActivity.this, "Please select Data Survey", Toast.LENGTH_SHORT).show();
        }

    }

    public void selectAllSurvey(boolean ind) {
        /*
        for(int n=0;n<list_survey.getCount();n++) {
            View v = list_survey.getChildAt(n);
            CheckBox chBox = (CheckBox) v.findViewById(R.id.checkBoxListSurvey);
            if (ind) chBox.setChecked(true);
            else chBox.setChecked(false);
            //x += chBox.isChecked() ? 1 : 0;
        }
        */
        for (int n = 0; n < listSurvey.length; n++) {
            if (ind) listSurvey[n].setCeklis(1);
            else listSurvey[n].setCeklis(0);
        }
        listSurvey();
    }


    public void submitToServer(final SavedSurvey survey) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        try {
            JSONObject params = new JSONObject(survey.getData());
            Log.d(TAG + " Request", new Gson().toJson(survey));

            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, apiAddress.submitForm + survey.getIdForm(),
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(SubmitSurveyActivity.this, "Data Survey " + Integer.toString(survey.getId()) + " successfully submited", Toast.LENGTH_SHORT).show();
                            //db.deleteSurvey(survey);
                            db.updateSubmited(survey);
                            getDataFromDB();
                            listSurvey();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SubmitSurveyActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    User user = userLocalStore.getLoggedInUser();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("User-agent", "My useragent");
                    String auth = "Bearer " + user.oAuth;
                    headers.put("Authorization", auth);
                    Log.d(TAG + " Authorization", auth);
                    return headers;
                }

            };
            requestQueue.add(stringRequest);

        } catch (JSONException e) {

        }

        //Toast.makeText(OpenFormActivity.this, params.toString(), Toast.LENGTH_LONG).show();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.submitButtonSelectAll):

                selectAllSurvey(true);

                break;
            case (R.id.buttonSubmitDisselect):

                selectAllSurvey(false);

                break;
            case (R.id.submitButtonSubmit):

                int n = list_survey.getCount();
                //Toast.makeText(SubmitSurveyActivity.this, "list survey : " + Integer.toString(n), Toast.LENGTH_SHORT).show();
                //View vnot = list_survey.getChildAt(n-1);
                //CheckBox chBoxt = (CheckBox) vnot.findViewById(R.id.checkBoxListSurvey);
                //SavedSurvey surveyt = (SavedSurvey) list_survey.getItemAtPosition(n);
                //Toast.makeText(SubmitSurveyActivity.this, "id survey : " + Integer.toString(surveyt.getId()), Toast.LENGTH_SHORT).show();
                submitSurvey();

                break;
            default:

        }

    }
}


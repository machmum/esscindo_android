package com.example.rhz.velosurvey.DataLocal;

/**
 * Created by RHZ on 4/4/2016.
 */
public class SavedSurvey {
    private int id;
    private String idForm;
    private String nameForm;
    private String data;
    private int submited;
    private int ceklis;

    public SavedSurvey() {
    }

    public SavedSurvey(int id, String idForm, String nameForm, String data, int submited) {
        this.id = id;
        this.idForm = idForm;
        this.data = data;
        this.nameForm = nameForm;
        this.submited = submited;
        this.ceklis = 0;
    }

    public void setCeklis(int c) {ceklis = c;}

    public int getCeklis() {return ceklis;}

    public String getNameForm() {
        return nameForm;
    }

    public void setNameForm(String nameForm) {
        this.nameForm = nameForm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdForm() {
        return idForm;
    }

    public void setIdForm(String idForm) {
        this.idForm = idForm;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getSubmited() {return submited;}

    public void setSubmited(int submited) {this.submited = submited;}
}

package com.example.rhz.velosurvey;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.rhz.velosurvey.UserData.User;

/**
 * Created by RHZ on 2/13/2016.
 */
public class UserLocalStore {
    public  static final String SP_NAME = "userAccount";
    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context) {
        userLocalDatabase = context.getSharedPreferences(SP_NAME,0);
    }

    public void storeUserData(User user) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("userName",user.userName);
        spEditor.putString("passWord",user.passWord);
        spEditor.putString("oAuth",user.oAuth);
        spEditor.putInt("userId",user.userId);
        spEditor.commit();
    }

    public User getLoggedInUser() {
        int userId = userLocalDatabase.getInt("userId",0);
        String userName = userLocalDatabase.getString("userName", "");
        String passWord = userLocalDatabase.getString("passWord","");
        String oAuth = userLocalDatabase.getString("oAuth","");



        User user = new User(userId,userName,passWord,oAuth);
        return  user;
    }


    public  void setLoggedInUser(boolean loggedIn) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean("loggedIn",loggedIn);
        spEditor.commit();
    }

    public void clearUserData() {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }


}

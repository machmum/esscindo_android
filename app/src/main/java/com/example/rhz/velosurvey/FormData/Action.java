package com.example.rhz.velosurvey.FormData;

import java.util.List;

/**
 * Created by RHZ on 3/22/2016.
 */
public class Action {
    public String name;
    public String value;
    public List<TargetField> actionFields;
    public Operator operator;
    public Operand operand;

    public Action(String name, String value, List<TargetField> actionFields, Operator operator, Operand operand) {
        this.name = name;
        this.value = value;
        this.actionFields = actionFields;
        this.operand = operand;
        this.operator = operator;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Operand getOperand() {
        return operand;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<TargetField> getActionFields() {
        return actionFields;
    }

    public void setActionFields(List<TargetField> actionFields) {
        this.actionFields = actionFields;
    }
}

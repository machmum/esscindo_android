package com.example.rhz.velosurvey.UserData;

/**
 * Created by RHZ on 2/10/2016.
 */
public class User {
    public int userId;
    public String userName;
    public String passWord;
    public String oAuth;

    public User(int userId, String userName, String passWord, String oAuth) {
        this.userId = userId;
        this.userName = userName;
        this.passWord = passWord;
        this.oAuth = oAuth;
    }
}

package com.example.rhz.velosurvey.DataLocal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RHZ on 4/4/2016.
 */
public class DBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "surveyData";

    private static final String TABLE_FORM = "form";

    private static final String TABLE_SURVEY = "survey";

    private static final String KEY_ID = "id";

    private static final String KEY_ID_FORM = "id_form";

    private static final String KEY_NAME = "name";

    private static final String KEY_BODY = "body";

    private static final String KEY_DATA = "data";

    private static final String KEY_ID_USER = "id_user";

    private static final String KEY_SUBMITED = "submited";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FORM_TABLE = "CREATE TABLE " + TABLE_FORM + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ID_FORM + " TEXT,"
                + KEY_NAME + " TEXT," + KEY_BODY + " TEXT," + KEY_ID_USER + " INTEGER" + ")";
        db.execSQL(CREATE_FORM_TABLE);
        String CREATE_SURVEY_TABLE = "CREATE TABLE " + TABLE_SURVEY + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ID_FORM + " TEXT,"
                + KEY_DATA + " TEXT," + KEY_ID_USER + " INTEGER," + KEY_SUBMITED + " INTEGER" + ")";
        db.execSQL(CREATE_SURVEY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEY);

        onCreate(db);
    }

    public void addForm(SavedForm form, int user_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID_FORM, form.getIdForm());
        values.put(KEY_NAME, form.getName());
        values.put(KEY_BODY, form.getBody());
        values.put(KEY_ID_USER, user_id);

        db.insert(TABLE_FORM, null, values);
        db.close();
    }

    public List<SavedForm> getAllForms(int user_id) {
        List<SavedForm> forms = new ArrayList<SavedForm>();

        String selectQuery = "SELECT * FROM " + TABLE_FORM + " WHERE " + KEY_ID_USER + " = " + user_id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                SavedForm form = new SavedForm();
                form.setId(Integer.parseInt(cursor.getString(0)));
                form.setIdForm(cursor.getString(1));
                form.setName(cursor.getString(2));
                form.setBody(cursor.getString(3));

                forms.add(form);
            } while (cursor.moveToNext());
        }

        return forms;
    }

    public void addSurvey(SavedSurvey survey, int user_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID_FORM, survey.getIdForm());
        values.put(KEY_DATA, survey.getData());
        values.put(KEY_ID_USER, user_id);
        values.put(KEY_SUBMITED, 0);

        db.insert(TABLE_SURVEY, null, values);
        db.close();
    }

    public void deleteAllForm(int user_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_FORM + " WHERE " + KEY_ID_USER + " = " + user_id);
        db.close();
    }

    public List<SavedSurvey> getAllSurvey(int user_id) {
        List<SavedSurvey> surveys = new ArrayList<SavedSurvey>();

        String selectQuery = "SELECT * FROM " + TABLE_SURVEY + " WHERE " + KEY_ID_USER + " = " + user_id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                SavedSurvey survey = new SavedSurvey();
                survey.setId(Integer.parseInt(cursor.getString(0)));
                survey.setIdForm(cursor.getString(1));
                survey.setData(cursor.getString(2));
                survey.setSubmited(Integer.parseInt(cursor.getString(4)));

                surveys.add(survey);
            } while(cursor.moveToNext());
        }

        return surveys;
    }

    public SavedSurvey getSurvey(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SURVEY, new String[]{KEY_ID, KEY_ID_FORM,
                        KEY_DATA,KEY_SUBMITED}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null,null);
        if (cursor != null)
            cursor.moveToFirst();
        String formId = cursor.getString(1);
        String formName = getFormNameById(formId);
        SavedSurvey survey = new SavedSurvey(Integer.parseInt(cursor.getString(0)),formId,formName,
                cursor.getString(2),Integer.parseInt(cursor.getString(3)));
        return survey;
    }

    public SavedForm getForm(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_FORM, new String[]{KEY_ID, KEY_ID_FORM,
                        KEY_NAME, KEY_BODY}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        SavedForm form = new SavedForm(Integer.parseInt(cursor.getString(0)),cursor.getString(1),
                cursor.getString(2), cursor.getString(3));

        return form;
    }

    public SavedForm getFormByIdLocal(String id) {

        //SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_FORM + " WHERE " + TABLE_FORM + "." + KEY_ID_FORM + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{id});

        SavedForm form = null;

        if(cursor.moveToFirst()) {
            form = new SavedForm(Integer.parseInt(cursor.getString(0)),cursor.getString(1),
                    cursor.getString(2), cursor.getString(3));
        }

        return form;
    }

    public int updateForm(SavedForm form) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, form.getName());
        values.put(KEY_BODY, form.getBody());

        return db.update(TABLE_FORM, values, KEY_ID + " = ?",
                new String[]{String.valueOf(form.getId())});
    }

    public int updateSurvey(SavedSurvey survey) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATA, survey.getData());

        return db.update(TABLE_SURVEY, values, KEY_ID + " = ?",
                new String[]{String.valueOf(survey.getId())});
    }

    public int updateSubmited(SavedSurvey survey) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_SUBMITED, 1);

        return db.update(TABLE_SURVEY, values, KEY_ID + " = ?",
                new String[]{String.valueOf(survey.getId())});
    }

    public void deleteSurvey(SavedSurvey survey) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SURVEY, KEY_ID + " = ?",
                new String[]{String.valueOf(survey.getId())});
        db.close();
    }

    public String getFormNameById(String id) {
        String name = "";

        String selectQuery = "SELECT * FROM " + TABLE_FORM + " WHERE " + TABLE_FORM + "." + KEY_ID_FORM + " =?";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, new String[]{id});

        if(cursor.moveToFirst()) {
            name = cursor.getString(2);
        }

        return name;
    }

    public void deleteForm(SavedForm form) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FORM, KEY_ID + " = ?",
                new String[] { String.valueOf(form.getId()) });
        db.close();
    }
}

package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/21/2016.
 */
public class Field {
    public String name;
    public String tagName;
    public String type;
    public String id;
    public String label;
    public String value;
    public String placeholder;
    public int lineNumber;
    public boolean required;
    public String groupLabel;
    public boolean checked;
    public int selectedOption;
    public String[] optionValues;
    public String[] optionLabels;
    public boolean readonly;

    public String[] getOptionLabels() {
        return optionLabels;
    }

    public void setOptionLabels(String[] optionLabels) {
        this.optionLabels = optionLabels;
    }

    public Field() {
        selectedOption = 0;
        name = "";
        tagName = "";
        type = "";
        id = "";
        label = "";
        value = "";
        placeholder = "";
        lineNumber = 0;
    }

    public Field(String name, String tagName, String id, String label) {
        this.name = name;
        this.tagName = tagName;
        this.type = "";
        this.id = id;
        this.label = label;
        this.value = "";
        this.lineNumber = 0;
        this.groupLabel = "";
        this.placeholder = "";
        this.required = false;
        this.checked = false;
        this.optionValues = null;
        this.optionLabels = null;
        this.selectedOption = 0;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean isChecked() {
        return checked;
    }

    public int getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(int selectedOption) {
        this.selectedOption = selectedOption;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String[] getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(String[] optionValues) {
        this.optionValues = optionValues;
    }

    public String getGroupLabel() {
        return groupLabel;
    }

    public void setGroupLabel(String groupLabel) {
        this.groupLabel = groupLabel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }
}

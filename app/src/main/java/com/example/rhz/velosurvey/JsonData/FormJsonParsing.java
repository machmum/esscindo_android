package com.example.rhz.velosurvey.JsonData;

import com.example.rhz.velosurvey.FormData.Form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by RHZ on 3/22/2016.
 */
public class FormJsonParsing {
    public FormJsonParsing() {

    }
    public Form[] parseFormJson(JSONArray jsonForms) {
        Form[] forms = new Form[jsonForms.length()];
        try {
            for (int n = 0; n < jsonForms.length(); n++) {
                JSONObject form = jsonForms.getJSONObject(n);
                forms[n] = new Form(form.getString("id"),form.getString("name"));
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return forms;
    }
}

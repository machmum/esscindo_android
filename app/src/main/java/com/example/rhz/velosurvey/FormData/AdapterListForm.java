package com.example.rhz.velosurvey.FormData;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.rhz.velosurvey.R;

import org.w3c.dom.Text;

/**
 * Created by RHZ on 2/16/2016.
 */
public class AdapterListForm extends ArrayAdapter<Form> {

    private Activity context;
    private Form[] forms;


    public AdapterListForm(Activity context, Form[] forms) {
        super(context, R.layout.name_form,forms);
        this.context = context;
        this.forms = forms;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.name_form, null, true);

        TextView textViewId = (TextView) listViewItem.findViewById(R.id.textViewId);
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);

        textViewId.setText("id form : " + forms[position].getId());
        textViewName.setText(forms[position].getName());

        return listViewItem;
    }


}

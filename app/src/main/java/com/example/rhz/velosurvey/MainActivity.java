package com.example.rhz.velosurvey;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rhz.velosurvey.ApiData.ApiAddress;
import com.example.rhz.velosurvey.UserData.User;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    User user;
    UserLocalStore userLocalStore;
    private boolean loggedIn = false;
    private EditText userName, passWord;
    private Button loginButton;
    private ApiAddress apiAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userName = (EditText) findViewById(R.id.userName);
        passWord = (EditText) findViewById(R.id.passWord);
        loginButton = (Button) findViewById(R.id.loginButton);

        userLocalStore = new UserLocalStore(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("userAccount", Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean("loggedIn",false);

        if(loggedIn){
            Intent intent = new Intent(this, MainMenuActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void login() {
        final String username = userName.getText().toString();
        final String password = passWord.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, apiAddress.login,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject userData = new JSONObject(response);
                            String token = userData.getString("access_token");
                            int idUser = userData.getInt("user_id");
                            //Toast.makeText(MainActivity.this, token, Toast.LENGTH_LONG).show();
                            user = new User(idUser,username,password,token);
                            userLocalStore.storeUserData(user);
                            userLocalStore.setLoggedInUser(true);

                            SharedPreferences sharedPreferences = getSharedPreferences("userAccount", Context.MODE_PRIVATE);
                            loggedIn = sharedPreferences.getBoolean("loggedIn",false);
                            Intent intent = new Intent(MainActivity.this, MainMenuActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        catch (Exception ex) {
                            if(response.equals("\"Wrong password\"")) {
                                Toast.makeText(MainActivity.this, "Wrong password", Toast.LENGTH_LONG).show();
                            }
                            else if(response.equals("\"User not found\"")) {
                                Toast.makeText(MainActivity.this, "User not found", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(MainActivity.this, "External server error", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Connection error", Toast.LENGTH_LONG).show();
            }
        }){ @Override
            protected Map<String, String> getParams() {
            Map<String,String> params = new HashMap<String, String>();
            params.put("username",username);
            params.put("password",password);

            return params;
        }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        login();
    }
}

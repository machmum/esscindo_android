package com.example.rhz.velosurvey.FormData;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.rhz.velosurvey.DataLocal.SavedSurvey;
import com.example.rhz.velosurvey.R;

/**
 * Created by RHZ on 4/5/2016.
 */
public class AdapterSubmit extends ArrayAdapter<SavedSurvey>{

    private Activity context;
    private SavedSurvey[] surveys;
//    private CeklisStat[] ceklisStat;


    public AdapterSubmit(Activity context, SavedSurvey[] surveys) {
        super(context, R.layout.saved_list,surveys);
        this.context = context;
        this.surveys = surveys;
    /*    this.ceklisStat = new CeklisStat[surveys.length];
        for(int n=0;n<ceklisStat.length;n++) {
            ceklisStat[n] = new CeklisStat();
        } */
    }

    /*
    public CeklisStat[] getCeklisStat() {
        return ceklisStat;
    }
    */

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.saved_list, null, true);

        TextView textFormName = (TextView) listViewItem.findViewById(R.id.editFormNameList);
        TextView textNameForm = (TextView) listViewItem.findViewById(R.id.editFormNameForm);
        TextView textHighLight = (TextView) listViewItem.findViewById(R.id.editSurveyNameList);
        CheckBox checkBox = (CheckBox) listViewItem.findViewById(R.id.checkBoxListSurvey);

        checkBox.setOnCheckedChangeListener(new CeklisAction(position));

        textFormName.setText("form id : " + surveys[position].getIdForm());
        textNameForm.setText("name : " + surveys[position].getNameForm());
        String highlight = "Data Survey " + surveys[position].getId();
        if(1 == surveys[position].getSubmited()) {
            highlight += " (submited)";
        }
        textHighLight.setText(highlight);
        if (1 == surveys[position].getCeklis()) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }

        return listViewItem;
    }

    public class CeklisAction implements CompoundButton.OnCheckedChangeListener {
        private int position;
        public CeklisAction(int n) {
            position = n;
        }
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                surveys[position].setCeklis(1);
            }
            else {
                surveys[position].setCeklis(0);
            }
        }
    }

}

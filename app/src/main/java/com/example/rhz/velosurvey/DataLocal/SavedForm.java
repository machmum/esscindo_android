package com.example.rhz.velosurvey.DataLocal;

/**
 * Created by RHZ on 4/4/2016.
 */
public class SavedForm {
    private int id;
    private String idForm;
    private String name;
    private String body;

    public SavedForm() {
    }

    public SavedForm(int id, String idForm, String name, String body) {
        this.id = id;
        this.idForm = idForm;
        this.name = name;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdForm() {
        return idForm;
    }

    public void setIdForm(String idForm) {
        this.idForm = idForm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

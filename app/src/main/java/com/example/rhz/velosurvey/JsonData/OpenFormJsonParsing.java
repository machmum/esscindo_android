package com.example.rhz.velosurvey.JsonData;

import android.util.Log;

import com.example.rhz.velosurvey.FormData.Action;
import com.example.rhz.velosurvey.FormData.Operand;
import com.example.rhz.velosurvey.FormData.Operator;
import com.example.rhz.velosurvey.FormData.TargetField;
import com.example.rhz.velosurvey.FormData.Condition;
import com.example.rhz.velosurvey.FormData.ConditionRule;
import com.example.rhz.velosurvey.FormData.Field;
import com.example.rhz.velosurvey.FormData.OpenForm;
import com.example.rhz.velosurvey.FormData.Rule;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RHZ on 3/22/2016.
 */
public class OpenFormJsonParsing {
    public OpenFormJsonParsing() {

    }

    public OpenForm parseOpenFormJson(JSONObject jsonOpenForm) {
        OpenForm openForm = new OpenForm();
        try {
            String formId = jsonOpenForm.getString("id");
            openForm.setId(formId);

            JSONArray formFields = jsonOpenForm.getJSONArray("fields");
            Field[] fields = new Field[formFields.length()];
            for(int n=0;n<formFields.length();n++) {
                JSONObject field = formFields.getJSONObject(n);
//                openForm.setId( Integer.toString(n));
                fields[n] = new Field(field.getString("name"),field.getString("tagName"),
                        field.getString("id"),field.getString("label"));
                if (field.has("options")) {
                    JSONArray optionsJson = field.getJSONArray("options");
                    String[] values = new String[optionsJson.length()];
                    String[] labels = new String[optionsJson.length()];
                    for(int m=0;m<optionsJson.length();m++) {
                        JSONObject option =  optionsJson.getJSONObject(m);
                        if(option.has("selected")) fields[n].setSelectedOption(m);
                        values[m] = new String(option.getString("value"));
                        labels[m] = new String(option.getString("label"));

                    }
                    fields[n].setOptionValues(values);
                    fields[n].setOptionLabels(labels);
                }
                //fields[n].setRequired(field.getBoolean("required"));
                if (field.has("required")) {
                    fields[n].setRequired(true);
                }
                if (field.has("checked")) {
                    fields[n].setChecked(true);
                }
                if (field.has("lineNumber")) {
                    fields[n].setLineNumber(field.getInt("lineNumber"));
                }
                if (field.has("placeholder")) {
                    fields[n].setPlaceholder(field.getString("placeholder"));
                }
                if (field.has("type")) {
                    fields[n].setType(field.getString("type"));
                }
                if (field.has("value")) {
                    fields[n].setValue(field.getString("value"));
                }
                if (field.has("groupLabel")) {
                    fields[n].setGroupLabel(field.getString("groupLabel"));
                }
                if (field.has("readonly")){
                    fields[n].setReadonly(true);
                }
            }
            openForm.setFields(fields);

            JSONArray rulesJson = jsonOpenForm.getJSONArray("rules");
            Rule[] rules = new Rule[rulesJson.length()];
            for(int n=0;n<rulesJson.length();n++) {
                JSONObject rule = rulesJson.getJSONObject(n);

                JSONObject conditionsJson = rule.getJSONObject ("conditions");
                Condition conditions = new Condition();

                JSONArray conditionRulesJson = null;
                String type = "";
                if(conditionsJson.has("all")) {
                    conditionRulesJson = conditionsJson.getJSONArray("all");        //NEED UPDATE!!!!!
                    type = "all";
                } else if(conditionsJson.has("any")) {
                    conditionRulesJson = conditionsJson.getJSONArray("any");
                    type = "any";
                } else if(conditionsJson.has("none")) {
                    conditionRulesJson = conditionsJson.getJSONArray("none");
                    type = "none";
                }

                    ConditionRule[] conditionRules = new ConditionRule[conditionRulesJson.length()];

                    for(int l=0;l<conditionRulesJson.length();l++) {
                        JSONObject conditionRule = conditionRulesJson.getJSONObject(l);

                        conditionRules[l] = new ConditionRule(conditionRule.getString("name"),conditionRule.getString("operator"),conditionRule.getString("value"));
                    }

                    conditions = new Condition(type,conditionRules);


                JSONArray actionsJson = rule.getJSONArray("actions");

                Action[] actions = new Action[actionsJson.length()];

                for (int m=0;m<actionsJson.length();m++) {
                    JSONObject action = actionsJson.getJSONObject(m);

                    JSONArray actionFieldsJson = action.getJSONArray("fields");

                    List<TargetField> actionFields = new ArrayList<TargetField>();
                    Operand operand = new Operand();
                    Operator operator = new Operator();

                    //Toast.makeText(OpenFormActivity.this, targt.getId(), Toast.LENGTH_SHORT).show();

                    for (int l=0;l<actionFieldsJson.length();l++) {
                        JSONObject actionField = actionFieldsJson.getJSONObject(l);

                        if (actionField.getString("name").equals("targetField") || actionField.getString("name").equals("field")) {
                            actionFields.add(new TargetField(actionField.getString("name"),actionField.getString("value")));
                        }
                        else if(actionField.getString("name").equals("operator")) {
                            operator.setValue(actionField.getString("value"));
                        }
                        else if(actionField.getString("name").equals("operands")) {
                            JSONArray valueJson = actionField.getJSONArray("value");
                            String[] values = new String[valueJson.length()];


                            for(int i = 0, count = valueJson.length(); i< count; i++)
                            {
                                values[i] = valueJson.getString(i);
                            }

                            operand.setValues(values);
                        }
                    }

                    actions[m] = new Action(action.getString("name"),action.getString("value"),actionFields,operator,operand);

                }

                rules[n] = new Rule(conditions,actions);

            }

            openForm.setRules(rules);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return openForm;
    }
}

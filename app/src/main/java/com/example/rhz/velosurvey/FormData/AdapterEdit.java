package com.example.rhz.velosurvey.FormData;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.rhz.velosurvey.DataLocal.SavedSurvey;
import com.example.rhz.velosurvey.R;

/**
 * Created by RHZ on 4/5/2016.
 */
public class AdapterEdit extends ArrayAdapter<SavedSurvey>{

    private Activity context;
    private SavedSurvey[] surveys;


    public AdapterEdit(Activity context, SavedSurvey[] surveys) {
        super(context, R.layout.edit_list,surveys);
        this.context = context;
        this.surveys = surveys;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.edit_list, null, true);

        TextView textFormName = (TextView) listViewItem.findViewById(R.id.editFormNameList);
        TextView textNameForm = (TextView) listViewItem.findViewById(R.id.editSurveyNameForm);
        TextView textHighLight = (TextView) listViewItem.findViewById(R.id.editSurveyNameList);

        textFormName.setText("form id : " + surveys[position].getIdForm());
        textNameForm.setText("name : " + surveys[position].getNameForm());
        String highlight = "Data Survey " + surveys[position].getId();
        if(1 == surveys[position].getSubmited()) {
            highlight += " (submited)";
        }

        textHighLight.setText(highlight);

        return listViewItem;
    }
}

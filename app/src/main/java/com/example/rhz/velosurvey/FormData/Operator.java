package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 6/21/2016.
 */
public class Operator {
    private  String value;

    public Operator(String value) {
        this.value = value;
    }

    public Operator() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

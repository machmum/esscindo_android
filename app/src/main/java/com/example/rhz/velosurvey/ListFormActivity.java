package com.example.rhz.velosurvey;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.rhz.velosurvey.ApiData.ApiAddress;
import com.example.rhz.velosurvey.DataLocal.DBHandler;
import com.example.rhz.velosurvey.DataLocal.SavedForm;
import com.example.rhz.velosurvey.FormData.AdapterListForm;
import com.example.rhz.velosurvey.FormData.Form;
import com.example.rhz.velosurvey.FormData.OpenForm;
import com.example.rhz.velosurvey.JsonData.FormJsonParsing;
import com.example.rhz.velosurvey.UserData.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListFormActivity extends AppCompatActivity {
    UserLocalStore userLocalStore;
    User user;
    public ApiAddress apiAddress;
    public static ListView list_form;
    public FormJsonParsing jsonParsing = new FormJsonParsing();
    public Form[] listForm = null;
    DBHandler db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_form);
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();
        db = new DBHandler(this);

        listForm();


//        getDataFromServer();
//        Toast.makeText(ListFormActivity.this, leng, Toast.LENGTH_LONG).show();
//        Toast.makeText(ListFormActivity.this, listForm[0].getId(), Toast.LENGTH_LONG).show();

    }

    private void storeListForm(JSONArray jsonForm) {
        //this.listForm = jsonParsing.parseFormJson(jsonForm);

        Form[] forms = null;

        forms = jsonParsing.parseFormJson(jsonForm);

        db.deleteAllForm(user.userId);

        for(Form form : forms) {
            SavedForm savedForm = new SavedForm();
            savedForm.setIdForm(form.getId());
            savedForm.setName(form.getName());
            getFormFromServer(savedForm);
        }



    }

    private void setListForm(JSONObject jsonForm, SavedForm savedForm) {
    //        Toast.makeText(OpenFormActivity.this, fields.length, Toast.LENGTH_LONG).show();
        String body = jsonForm.toString();
        savedForm.setBody(body);

        db.addForm(savedForm,user.userId);
        listForm();

    //        String leng = String.valueOf(listForm.length);
    }

    public void getFormFromServer(final SavedForm form) {

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, apiAddress.openForm + form.getIdForm(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        setListForm(response,form);
                        //Toast.makeText(OpenFormActivity.this, "SavedForm updated", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(OpenFormActivity.this, "Connection failure", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                User user = userLocalStore.getLoggedInUser();
                String auth = "Bearer " + user.oAuth;
                headers.put("Authorization", auth);
                return headers;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonRequest);


    }

    private void getListFormFromDB() {
        List<SavedForm> forms = db.getAllForms(user.userId);
        listForm = new Form[forms.size()];

        int n = 0;

        for(SavedForm form : forms) {
            listForm[n] = new Form(form.getIdForm(),form.getName());
            listForm[n++].setIdLocal(form.getId());
        }

    }

    public void getListFormFromServer() {

        JsonArrayRequest jsonRequest = new JsonArrayRequest(Request.Method.GET, apiAddress.listForm,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        storeListForm(response);
                        Toast.makeText(ListFormActivity.this, "Form updated", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                User user = userLocalStore.getLoggedInUser();
                Toast.makeText(ListFormActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                User user = userLocalStore.getLoggedInUser();
                String auth = "Bearer "+ user.oAuth;
                headers.put("Authorization",auth);
                return headers;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonRequest);


    }

    public void startNewForm(Form form) {
        Intent intent = new Intent(ListFormActivity.this, OpenFormActivity.class);
        intent.putExtra("FormId", form.getId());
        intent.putExtra("FormIdLocal",form.getIdLocal());
        intent.putExtra("FormName", form.getName());
        startActivity(intent);
    }

    public void listForm() {
        list_form = (ListView) findViewById(R.id.listView);
        getListFormFromDB();
        AdapterListForm viewListForm = new AdapterListForm(ListFormActivity.this,listForm);

        list_form.setAdapter(viewListForm);
        list_form.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Form form = (Form) list_form.getItemAtPosition(position);
                        //Toast.makeText(ListFormActivity.this, form.getBody(), Toast.LENGTH_LONG).show();

                        startNewForm(form);
                    }
                }
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            //userLocalStore.clearUserData();

            getListFormFromServer();

            //onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


}

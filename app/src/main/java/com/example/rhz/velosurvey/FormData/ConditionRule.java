package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/28/2016.
 */
public class ConditionRule {
    public String name;
    public String operator;
    public String value;

    public ConditionRule() {
    }

    public ConditionRule(String name, String operator, String value) {
        this.name = name;
        this.operator = operator;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

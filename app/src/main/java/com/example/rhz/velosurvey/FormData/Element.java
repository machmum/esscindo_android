package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/22/2016.
 */
public class Element {
    private String name;
    private String operator;
    private int value;

    public Element(String name, String operator, int value) {
        this.name = name;
        this.operator = operator;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

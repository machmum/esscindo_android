package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 6/24/2016.
 */
public class MathRule {
    public MathRule() {
        valDob = 0.0;
        valInt = 0;
    }

    public int valInt;
    public double valDob;

    public MathRule(double valDob, int valInt) {
        this.valDob = valDob;
        this.valInt = valInt ;
    }

    public int getValInt() {
        return valInt;
    }

    public void setValInt(int valInt) {
        this.valInt = valInt;
    }

    public double getValDob() {
        return valDob;
    }

    public void setValDob(double valDob) {
        this.valDob = valDob;
    }
}

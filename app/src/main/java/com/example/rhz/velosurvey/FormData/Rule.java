package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/21/2016.
 */
public class Rule {
    private Condition conditions;
    private Action[] actions;

    public Rule() {
    }

    public Rule(Condition conditions, Action[] actions) {
        this.conditions = conditions;
        this.actions = actions;
    }

    public Condition getConditions() {
        return conditions;
    }

    public void setConditions(Condition conditions) {
        this.conditions = conditions;
    }

    public Action[] getActions() {
        return actions;
    }

    public void setActions(Action[] actions) {
        this.actions = actions;
    }
}

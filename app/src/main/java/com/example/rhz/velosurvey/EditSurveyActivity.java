package com.example.rhz.velosurvey;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rhz.velosurvey.DataLocal.DBHandler;
import com.example.rhz.velosurvey.DataLocal.SavedSurvey;
import com.example.rhz.velosurvey.FormData.AdapterEdit;
import com.example.rhz.velosurvey.FormData.AdapterSubmit;
import com.example.rhz.velosurvey.UserData.User;

import java.util.List;

public class EditSurveyActivity extends AppCompatActivity {
    ListView list_survey;
    SavedSurvey[] listSurvey;
    DBHandler db;
    UserLocalStore userLocalStore;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_survey);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();
        db = new DBHandler(this);
        listSurvey();

        
    }

    public void getDataFromDB() {
        List<SavedSurvey> surveys = db.getAllSurvey(user.userId);
        listSurvey = new SavedSurvey[surveys.size()];
        int n = 0;
        for (SavedSurvey survey : surveys) {
            String formId = survey.getIdForm();
            String formName = db.getFormNameById(formId);
            listSurvey[n++] = new SavedSurvey(survey.getId(),formId,formName,survey.getData(),survey.getSubmited());
        }
    }

    public void startNewForm(SavedSurvey survey) {
        Intent intent = new Intent(EditSurveyActivity.this, OpenFormActivity.class);
        intent.putExtra("FormId", survey.getIdForm());
        intent.putExtra("SurveyId", survey.getId());
        intent.putExtra("FormIdLocal",survey.getIdForm());
        //intent.putExtra("FormName", survey.getName());
        startActivity(intent);
    }

    public void listSurvey() {
        list_survey = (ListView) findViewById(R.id.editSavedList);
        getDataFromDB();
        AdapterEdit adapter = new AdapterEdit(EditSurveyActivity.this,listSurvey);

        list_survey.setAdapter(adapter);
        list_survey.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SavedSurvey survey = (SavedSurvey) list_survey.getItemAtPosition(position);
                        startNewForm(survey);

                        //Toast.makeText(EditSurveyActivity.this, survey.getData(), Toast.LENGTH_LONG).show();

                        //startNewForm(form);
                    }
                }
        );

    }

}

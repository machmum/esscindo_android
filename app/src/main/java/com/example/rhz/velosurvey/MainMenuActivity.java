package com.example.rhz.velosurvey;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.rhz.velosurvey.ApiData.ApiAddress;
import com.example.rhz.velosurvey.DataLocal.DBHandler;
import com.example.rhz.velosurvey.DataLocal.SavedForm;
import com.example.rhz.velosurvey.FormData.Form;
import com.example.rhz.velosurvey.JsonData.FormJsonParsing;
import com.example.rhz.velosurvey.UserData.User;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener{
    Button listButton;
    Button editButton;
    Button deleteButton;
    Button submitButton;
    TextView textShowUserName;
    UserLocalStore userLocalStore;
    User user;
    public ApiAddress apiAddress;
    public FormJsonParsing jsonParsing = new FormJsonParsing();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userLocalStore = new UserLocalStore(this);

        user = userLocalStore.getLoggedInUser();
        textShowUserName = (TextView) findViewById(R.id.showUserName);
        textShowUserName.setText("username : " + user.userName);

        listButton = (Button) findViewById(R.id.buttonList);
        listButton.setOnClickListener(this);

        editButton = (Button) findViewById(R.id.buttonEdit);
        editButton.setOnClickListener(this);

        deleteButton = (Button) findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(this);

        submitButton = (Button) findViewById(R.id.buttonSubmit);
        submitButton.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.buttonList) :
                Intent intent = new Intent(MainMenuActivity.this, ListFormActivity.class);
                startActivity(intent);
                break;
            case (R.id.buttonEdit) :
                Intent intent3 = new Intent(MainMenuActivity.this, EditSurveyActivity.class);
                startActivity(intent3);
                break;
            case (R.id.buttonDelete) :
                Intent intent2 = new Intent(MainMenuActivity.this, DeleteSurveyActivity.class);
                startActivity(intent2);
                break;
            case (R.id.buttonSubmit) :
                Intent intent1 = new Intent(MainMenuActivity.this, SubmitSurveyActivity.class);
                startActivity(intent1);
                break;
            default:
                break;

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            userLocalStore.clearUserData();

            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


}

package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 2/15/2016.
 */
public class Form {
    private String id;
    private int idLocal;
    private String name;


    public Form(String id, String name) {
        this.id = id;
        this.idLocal = 0;
        this.name = name;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    public String getName(){
        return this.name;
    }
    public String getId(){
        return this.id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setId(String id) {
        this.id = id;
    }
}

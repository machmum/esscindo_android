package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 6/21/2016.
 */
public class Operand {
    String[] values;

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public Operand(String[] values) {
        this.values = values;
    }

    public Operand() {
        values = null;
    }
}

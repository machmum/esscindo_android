package com.example.rhz.velosurvey.ApiData;

/**
 * Created by RHZ on 3/15/2016.
 */
public class ApiAddress {
    public static  final String login = "https://www.esscindo.com/login/api";
    public static  final String listForm = "https://www.esscindo.com/api/forms";
    public static final String openForm ="https://www.esscindo.com/api/form?id=";
    public static final String testingGet = "https://www.httpbin.org/get";
    public static final String testingPost = "https://www.httpbin.org/post";
    public static final String submitForm = "https://www.esscindo.com/api/submit?id=";
}

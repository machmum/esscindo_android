package com.example.rhz.velosurvey;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rhz.velosurvey.DataLocal.DBHandler;
import com.example.rhz.velosurvey.DataLocal.SavedSurvey;
import com.example.rhz.velosurvey.FormData.AdapterSubmit;
import com.example.rhz.velosurvey.UserData.DeleteDialog;
import com.example.rhz.velosurvey.UserData.User;

import java.util.List;

public class DeleteSurveyActivity extends AppCompatActivity implements View.OnClickListener{
    ListView list_survey;
    SavedSurvey[] listSurvey;
    DBHandler db;
    UserLocalStore userLocalStore;
    User user;
    AdapterSubmit adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_saved_survey);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();
        db = new DBHandler(this);

        Button deleteButton = (Button) findViewById(R.id.deleteButtonDelete);
        Button selectAllButton = (Button) findViewById(R.id.deleteButtonSelectAll);
        Button disselectAllButton = (Button) findViewById(R.id.buttonDeleteDisselect);
        deleteButton.setOnClickListener(this);
        selectAllButton.setOnClickListener(this);
        disselectAllButton.setOnClickListener(this);
        getDataFromDB();
        listSurvey();


    }

    public void getDataFromDB() {
        List<SavedSurvey> surveys = db.getAllSurvey(user.userId);
        listSurvey = new SavedSurvey[surveys.size()];
        int n = 0;
        for (SavedSurvey survey : surveys) {
            String formId = survey.getIdForm();
            String formName = db.getFormNameById(formId);
            listSurvey[n++] = new SavedSurvey(survey.getId(),formId,formName,survey.getData(),survey.getSubmited());
        }
    }

    public void listSurvey() {
        list_survey = (ListView) findViewById(R.id.deleteList);
        adapter = new AdapterSubmit(DeleteSurveyActivity.this,listSurvey);

        list_survey.setAdapter(adapter);
        list_survey.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SavedSurvey survey = (SavedSurvey) list_survey.getItemAtPosition(position);
                        Toast.makeText(DeleteSurveyActivity.this, survey.getData(), Toast.LENGTH_LONG).show();

                        //startNewForm(form);
                    }
                }
        );

    }

    public void deleteSurvey() {
        int x = 0;
        /*for(int n=0;n<list_survey.getCount();n++) {
            View v = list_survey.getChildAt(n);
            CheckBox chBox = (CheckBox) v.findViewById(R.id.checkBoxListSurvey);
            //x += chBox.isChecked() ? 1 : 0;
            if (chBox.isChecked()) {
                SavedSurvey survey = (SavedSurvey) list_survey.getItemAtPosition(x);
                db.deleteSurvey(survey);
                //chBox.setActivated(false);
                chBox.setClickable(false);

                x++;
            }
        } */
        for(int n=0;n<listSurvey.length;n++) {
            SavedSurvey survey = listSurvey[n];
            if (1 == survey.getCeklis()) {
                db.deleteSurvey(survey);

                x++;
            }
        }
        getDataFromDB();
        listSurvey();
        if (0 == x){
            Toast.makeText(DeleteSurveyActivity.this, "Please select Data Survey", Toast.LENGTH_SHORT).show();

        }
        else {
            Toast.makeText(DeleteSurveyActivity.this, "Survey deleted : " + Integer.toString(x), Toast.LENGTH_SHORT).show();
        }
    }

    public void selectAllSurvey(boolean ind) {
        for(int n=0;n<listSurvey.length;n++) {
            if (ind) listSurvey[n].setCeklis(1);
            else listSurvey[n].setCeklis(0);
        }
        listSurvey();
    }



    public void confirmDelete() {
        final AlertDialog.Builder builder = new DeleteDialog(DeleteSurveyActivity.this);
        //builder.setTitle(R.string.app_name);
        builder.setMessage("Delete survey ?");
        //builder.setIcon(R.drawable.ic_launcher);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteSurvey();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();

        alert.show();
        //Toast.makeText(DeleteSurveyActivity.this, "Confirm = " + Integer.toString(confirm), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.deleteButtonSelectAll) :
                selectAllSurvey(true);
                break;
            case (R.id.buttonDeleteDisselect) :
                selectAllSurvey(false);
                break;
            case (R.id.deleteButtonDelete) :
                confirmDelete();
                break;
            default:
        }
    }
}

package com.example.rhz.velosurvey;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.rhz.velosurvey.ApiData.ApiAddress;
import com.example.rhz.velosurvey.DataLocal.DBHandler;
import com.example.rhz.velosurvey.DataLocal.SavedForm;
import com.example.rhz.velosurvey.DataLocal.SavedSurvey;
import com.example.rhz.velosurvey.FormAction.FieldFunction;
import com.example.rhz.velosurvey.FormData.MathRule;
import com.example.rhz.velosurvey.FormData.Operand;
import com.example.rhz.velosurvey.FormData.Operator;
import com.example.rhz.velosurvey.FormData.TargetField;
import com.example.rhz.velosurvey.FormData.Condition;
import com.example.rhz.velosurvey.FormData.ConditionRule;
import com.example.rhz.velosurvey.FormData.Field;
import com.example.rhz.velosurvey.FormData.OpenCondition;
import com.example.rhz.velosurvey.FormData.OpenForm;
import com.example.rhz.velosurvey.FormData.Rule;
import com.example.rhz.velosurvey.JsonData.OpenFormJsonParsing;
import com.example.rhz.velosurvey.UserData.User;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.example.rhz.velosurvey.FormData.Action;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class OpenFormActivity extends FragmentActivity implements View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();

    UserLocalStore userLocalStore;
    public ApiAddress apiAddress;
    private LinearLayout formArea;
    private FieldFunction fieldFunction = new FieldFunction();
    private TextView formTitle;
    private Button saveButton;
    private Button nextButton;
    private Button prevButton;
    private LinearLayout bodyForm;
    private ScrollView scrollForm;
    private ViewPager pagerForm;
    private PagerAdapter pagerFormAdapter;
    private int NUM_PAGES;

    OpenFormJsonParsing jsonParsing;
    OpenForm openForm;
    int surveyId = 0;
    DBHandler db;
    Map<Field, Integer> elementName = new HashMap<Field, Integer>();
    Map<Field, Integer> elementLabel = new HashMap<Field, Integer>();
    List<String> forbidenField = new ArrayList<String>();
//    Map<String,Field> elementData = new HashMap<String,Field>();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private long mOnStartTimeStamp = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_form1);
        db = new DBHandler(this);
        userLocalStore = new UserLocalStore(this);
        formTitle = (TextView) findViewById(R.id.titleForm1);
//        submitButton = (Button) findViewById(R.id.submitButton);
        saveButton = (Button) findViewById(R.id.saveButton1);
        saveButton.setOnClickListener(this);

        /*
        nextButton = (Button) findViewById(R.id.openformNextButton);
        nextButton.setOnClickListener(this);
        prevButton = (Button) findViewById(R.id.openformPrevButton);
        prevButton.setOnClickListener(this);
        */


/*
        pagerForm = (ViewPager) findViewById(R.id.pagerForm);
        pagerFormAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        pagerForm.setAdapter(pagerFormAdapter);
*/
        //NUM_PAGES = 1;

        jsonParsing = new OpenFormJsonParsing();
        //    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //    setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            String formId = (String) b.get("FormId");
            String formName = (String) b.get("FormName");
            int formIdLocal = (Integer) b.getInt("FormIdLocal");

            formTitle.setText(formName);

            getDataFromId(formId);

            if (b.containsKey("SurveyId")) {
                surveyId = (Integer) b.get("SurveyId");
                setSavedSurveyData(surveyId);
            }

            //getDataFromServer(formId);
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void setSavedSurveyData(int id) {
        Field[] fields = openForm.getFields();
        SavedSurvey savedData = db.getSurvey(id);
        try {
            JSONObject surveyData = new JSONObject(savedData.getData());
            //Toast.makeText(OpenFormActivity.this, surveyData.toString(), Toast.LENGTH_LONG).show();
            for (int n = 0; n < fields.length; n++) {
                if ((fields[n].getTagName()).equals("input")) {
                    if ((fields[n].getType()).equals("radio")) {
                        String radName = fields[n].getName();
                        //boolean fir = true;
                        int firId = n;
                        RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(fields[n]));
                        int run = 0;
                        while (fields[n].getName().equals(radName)) {
                            RadioButton radioButton = (RadioButton) radioGroup.getChildAt(run);
                            if (fields[n].getValue().equals(surveyData.getString(fields[n].getName()))) {
                                radioButton.setChecked(true);
                                //fir = false;
                            } else {
                                radioButton.setChecked(false);
                            }
                            run++;
                            n++;
                            if (n == fields.length) break;
                        }

                        n--;

                    } else if ((fields[n].getType()).equals("checkbox")) {
                        if (surveyData.has(fields[n].getName())) {
                            JSONArray chVal = surveyData.getJSONArray(fields[n].getName());

                            String clName = fields[n].getName();
                            while (fields[n].getName().equals(clName)) {
                                CheckBox checkBox = (CheckBox) findViewById(elementName.get(fields[n]));
                                for (int m = 0; m < chVal.length(); m++) {
                                    if (chVal.getString(m).equals(fields[n].getValue())) {
                                        checkBox.setChecked(true);
                                    }
                                }


                                n++;
                                if (n == fields.length) break;
                            }
                            n--;
                        }
                    } else {
                        //Toast.makeText(OpenFormActivity.this, fields[n].getName(), Toast.LENGTH_LONG).show();
                        if (surveyData.has(fields[n].getName())) {
                            //Toast.makeText(OpenFormActivity.this, Integer.toString(elementName.get(fields[n])), Toast.LENGTH_LONG).show();
                            EditText inputText = (EditText) findViewById(elementName.get(fields[n]));
                            inputText.setText(surveyData.getString(fields[n].getName()));
                        }
                    }
                } else if ((fields[n].getTagName()).equals("textarea")) {
                    if (surveyData.has(fields[n].getName())) {
                        //Toast.makeText(OpenFormActivity.this, Integer.toString(elementName.get(fields[n])), Toast.LENGTH_LONG).show();
                        EditText inputText = (EditText) findViewById(elementName.get(fields[n]));
                        inputText.setText(surveyData.getString(fields[n].getName()));
                    }

                } else if ((fields[n].getTagName()).equals("select")) {
                    Spinner select = (Spinner) findViewById(elementName.get(fields[n]));
                    JSONArray chVal = surveyData.getJSONArray(fields[n].getName());
                    String[] valueSelect = fields[n].getOptionValues();
                    for (int m = 0; m < select.getCount(); m++) {
                        if (chVal.getString(0).equals(valueSelect[m])) {
                            select.setSelection(m);
                            break;
                        }
                    }
                }
            }
        } catch (JSONException e) {

        }

    }

    private void setListForm(JSONObject jsonForm) {
        openForm = new OpenForm();
        openForm = jsonParsing.parseOpenFormJson(jsonForm);
//        Toast.makeText(OpenFormActivity.this, fields.length, Toast.LENGTH_LONG).show();
        buildForm();
//        String leng = String.valueOf(listForm.length);
    }

    private void clearElementValue(Field target) {
        if (target.getTagName().equals("textarea")) {
            EditText editText = (EditText) findViewById(elementName.get(target));
            editText.setText(target.getValue());
        } else if (target.getTagName().equals("input")) {
            if (target.getType().equals("radio")) {
                RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(target));

            } else if (target.getType().equals("checkbox")) {
                CheckBox checkBox = (CheckBox) findViewById(elementName.get(target));
                if (target.isChecked()) checkBox.setChecked(true);
                else checkBox.setChecked(false);
            } else {
                EditText editText = (EditText) findViewById(elementName.get(target));
                editText.setText(target.getValue());
            }
        } else if (target.getTagName().equals("select")) {
            Spinner spinner = (Spinner) findViewById(elementName.get(target));
            spinner.setSelection(target.getSelectedOption());
        }
    }

    private MathRule getAkumulationValue(Action action) {
        Operand operand = action.getOperand();
        Operator operator = action.getOperator();

        String[] values = operand.getValues();
        Field[] fields = openForm.getFields();

        //int[] val = new int[values.length];
        //int nVal = 0;

        List<MathRule> val = new ArrayList<MathRule>();

        for (int vn = 0; vn < values.length; vn++) {
            Field target = fieldFunction.getFieldByName(values[vn], fields);
            if (target.getType().equals("number")) {
                EditText editText = (EditText) findViewById(elementName.get(target));
                String text = editText.getText().toString();

                if (!(text.equals("") || text.isEmpty())) {
                    int elmVal = Integer.parseInt(text);
                    val.add(new MathRule((double) elmVal, elmVal));
                    //nVal++;
                }
            } else if (target.getType().equals("radio")) {
                RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(target));
                int selectedId = radioGroup.getCheckedRadioButtonId();
                View selectedRadio = radioGroup.findViewById(selectedId);
                int ind = radioGroup.indexOfChild(selectedRadio);
                int n = fieldFunction.getIdByField(target, fields);

                String text = fields[n + ind].getValue();
                if (!(text.equals("") || text.isEmpty())) {
                    int elmVal = Integer.parseInt(text);
                    val.add(new MathRule((double) elmVal, elmVal));
                    //nVal++;
                }
                //String text = radioGroup.get
            }
        }

        MathRule akm = new MathRule();

        if (operator.getValue().equals("*")) {
            akm = new MathRule(1, 1);
        }

        for (MathRule v : val) {
            if (operator.getValue().equals("+")) {
                akm.valInt += v.valInt;
            } else if (operator.getValue().equals("*")) {
                akm.valInt *= v.valInt;
            }
        }

        return akm;
    }

    private void setActionRule(Action action, Field[] fields) {
        List<TargetField> actionFields = action.getActionFields();
        for (TargetField actField : actionFields) {
            Field target = fieldFunction.getFieldByName(actField.getValue(), fields);
            if (target.getTagName().equals("textarea")) {
                EditText editText = (EditText) findViewById(elementName.get(target));
                if (action.getValue().equals("toShow")) {
                    target.setRequired(true);
                    editText.setVisibility(View.VISIBLE);
                } else if (action.getValue().equals("toHide")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    editText.setVisibility(View.GONE);
                } else if (action.getValue().equals("toDisable")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    editText.setEnabled(false);
                } else if (action.getValue().equals("toEnable")) {
                    target.setRequired(true);
                    editText.setEnabled(true);
                }
            } else if (target.getTagName().equals("input")) {
                if (target.getType().equals("radio")) {
                    RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(target));
                    if (action.getValue().equals("toShow")) {
                        target.setRequired(true);
                        radioGroup.setVisibility(View.VISIBLE);
                    } else if (action.getValue().equals("toHide")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        radioGroup.setVisibility(View.GONE);
                    } else if (action.getValue().equals("toDisable")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        radioGroup.setEnabled(false);
                    } else if (action.getValue().equals("toEnable")) {
                        target.setRequired(true);
                        radioGroup.setEnabled(true);
                    }
                } else if (target.getType().equals("checkbox")) {
                    List<Field> multiTarget = fieldFunction.getMultiFieldByName(actField.getValue(), fields);
                    //Toast.makeText(OpenFormActivity.this, Integer.toString(multiTarget.size()), Toast.LENGTH_SHORT).show();
                    for (Field targt : multiTarget) {
                        CheckBox checkBox = (CheckBox) findViewById(elementName.get(targt));
                        //Toast.makeText(OpenFormActivity.this, targt.getId(), Toast.LENGTH_SHORT).show();
                        if (action.getValue().equals("toShow")) {
                            target.setRequired(true);
                            checkBox.setVisibility(View.VISIBLE);
                        } else if (action.getValue().equals("toHide")) {
                            clearElementValue(target);
                            target.setRequired(false);
                            checkBox.setVisibility(View.GONE);
                        } else if (action.getValue().equals("toDisable")) {
                            clearElementValue(target);
                            target.setRequired(false);
                            checkBox.setEnabled(false);
                        } else if (action.getValue().equals("toEnable")) {
                            target.setRequired(true);
                            checkBox.setEnabled(true);
                        }
                    }
                } else {
                    EditText editText = (EditText) findViewById(elementName.get(target));
                    if (action.getValue().equals("toShow")) {
                        target.setRequired(true);
                        editText.setVisibility(View.VISIBLE);
                    } else if (action.getValue().equals("toHide")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        editText.setVisibility(View.GONE);
                    } else if (action.getValue().equals("toDisable")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        editText.setEnabled(false);
                    } else if (action.getValue().equals("toEnable")) {
                        target.setRequired(true);
                        editText.setEnabled(true);
                    } else if (action.getValue().equals("performArithmeticOperations")) {
                        MathRule akm = new MathRule();

                        akm = getAkumulationValue(action);

                        editText.setText(Integer.toString(akm.getValInt()));
                        editText.setEnabled(false);
                    }
                }
            } else if (target.getTagName().equals("select")) {
                Spinner spinner = (Spinner) findViewById(elementName.get(target));
                if (action.getValue().equals("toShow")) {
                    target.setRequired(true);
                    spinner.setVisibility(View.VISIBLE);
                } else if (action.getValue().equals("toHide")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    spinner.setVisibility(View.GONE);
                } else if (action.getValue().equals("toDisable")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    spinner.setEnabled(false);
                } else if (action.getValue().equals("toEnable")) {
                    target.setRequired(true);
                    spinner.setEnabled(true);
                }
            }

            TextView label = (TextView) findViewById(elementLabel.get(target));
            if (action.getValue().equals("toShow")) {
                label.setVisibility(View.VISIBLE);
            } else if (action.getValue().equals("toHide")) {
                label.setVisibility(View.GONE);
            }
        }

    }

    private void initiateActionsRule(Action action, Field[] fields) {
        List<TargetField> actionFields = action.getActionFields();
        for (TargetField actField : actionFields) {
            Field target = fieldFunction.getFieldByName(actField.getValue(), fields);
            if (target.getTagName().equals("textarea")) {
                EditText editText = (EditText) findViewById(elementName.get(target));
                if (action.getValue().equals("toShow")) {
                    target.setRequired(false);
                    clearElementValue(target);
                    editText.setVisibility(View.GONE);
                } else if (action.getValue().equals("toHide")) {
                    target.setRequired(true);
                    editText.setVisibility(View.VISIBLE);
                } else if (action.getValue().equals("toEnable")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    editText.setEnabled(false);
                } else if (action.getValue().equals("toDisable")) {
                    target.setRequired(true);
                    editText.setEnabled(true);
                }
            } else if (target.getTagName().equals("input")) {
                if (target.getType().equals("radio")) {
                    RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(target));
                    if (action.getValue().equals("toShow")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        radioGroup.setVisibility(View.GONE);
                    } else if (action.getValue().equals("toHide")) {
                        target.setRequired(true);
                        radioGroup.setVisibility(View.VISIBLE);
                    } else if (action.getValue().equals("toEnable")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        radioGroup.setEnabled(false);
                    } else if (action.getValue().equals("toDisable")) {
                        target.setRequired(true);
                        radioGroup.setEnabled(true);
                    }
                } else if (target.getType().equals("checkbox")) {
                    List<Field> multiTarget = fieldFunction.getMultiFieldByName(actField.getValue(), fields);
                    //Toast.makeText(OpenFormActivity.this, Integer.toString(multiTarget.size()), Toast.LENGTH_SHORT).show();
                    for (Field targt : multiTarget) {
                        CheckBox checkBox = (CheckBox) findViewById(elementName.get(targt));
                        //Toast.makeText(OpenFormActivity.this, targt.getId(), Toast.LENGTH_SHORT).show();
                        if (action.getValue().equals("toShow")) {
                            clearElementValue(target);
                            target.setRequired(false);
                            checkBox.setVisibility(View.GONE);
                        } else if (action.getValue().equals("toHide")) {
                            target.setRequired(true);
                            checkBox.setVisibility(View.VISIBLE);
                        } else if (action.getValue().equals("toEnable")) {
                            clearElementValue(target);
                            target.setRequired(false);
                            checkBox.setEnabled(false);
                        } else if (action.getValue().equals("toDisable")) {
                            target.setRequired(true);
                            checkBox.setEnabled(true);
                        }
                    }
                } else {
                    EditText editText = (EditText) findViewById(elementName.get(target));
                    if (action.getValue().equals("toShow")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        editText.setVisibility(View.GONE);
                    } else if (action.getValue().equals("toHide")) {
                        target.setRequired(true);
                        editText.setVisibility(View.VISIBLE);
                    } else if (action.getValue().equals("toEnable")) {
                        clearElementValue(target);
                        target.setRequired(false);
                        editText.setEnabled(false);
                    } else if (action.getValue().equals("toDisable")) {
                        target.setRequired(true);
                        editText.setEnabled(true);
                    }
                }
            } else if (target.getTagName().equals("select")) {
                Spinner spinner = (Spinner) findViewById(elementName.get(target));
                if (action.getValue().equals("toShow")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    spinner.setVisibility(View.GONE);
                } else if (action.getValue().equals("toHide")) {
                    target.setRequired(true);
                    spinner.setVisibility(View.VISIBLE);
                } else if (action.getValue().equals("toEnable")) {
                    clearElementValue(target);
                    target.setRequired(false);
                    spinner.setEnabled(false);
                } else if (action.getValue().equals("toDisable")) {
                    target.setRequired(true);
                    spinner.setEnabled(true);
                }
            }
            TextView label = (TextView) findViewById(elementLabel.get(target));
            if (action.getValue().equals("toShow")) {
                label.setVisibility(View.GONE);
            } else if (action.getValue().equals("toHide")) {
                label.setVisibility(View.VISIBLE);
            }
        }

    }

    private String checkElementConditionValue(Field target, int id) {
        String rtrn = "";
        Field[] fields = openForm.getFields();

        if (target.getTagName().equals("textarea")) {
            EditText editText = (EditText) findViewById(elementName.get(target));
            rtrn = editText.getText().toString();
        } else if (target.getTagName().equals("input")) {
            if (target.getType().equals("radio")) {
                RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(target));
                int radioId = radioGroup.getCheckedRadioButtonId();

                if (radioId >= 0) {
                    RadioButton radioButton = (RadioButton) findViewById(radioId);
                    int idx = radioGroup.indexOfChild(radioButton);
                    int idTarget = fieldFunction.getIdByField(target, fields);
                    //Field field = fieldFunction.getFieldById(radioId,fields);
                    /**/
                    if (idTarget >= 0) rtrn = (String) fields[idTarget + idx].getValue();
                }

                //radioGroup.setVisibility(View.GONE);
            } else if (target.getType().equals("checkbox")) {
                CheckBox checkBox = (CheckBox) findViewById(elementName.get(target));
                if (checkBox.isChecked()) rtrn = "T";
                else rtrn = "F";
            } else {
                EditText editText = (EditText) findViewById(elementName.get(target));
                rtrn = editText.getText().toString();
            }
        } else if (target.getTagName().equals("select")) {
            Spinner spinner = (Spinner) findViewById(elementName.get(target));
            String[] values = target.getOptionValues();
            int rtrnInd = spinner.getSelectedItemPosition();
            rtrn = values[rtrnInd];
            //spinner.setVisibility(View.GONE);
        }

        return rtrn;
    }

    private void checkElementCondition(OpenCondition target) {
        Field[] fields = openForm.getFields();
        List<Rule> rules = target.getRules();

        //Toast.makeText(OpenFormActivity.this, "rule get", Toast.LENGTH_SHORT).show();
        for (Rule rule : rules) {
            Condition condition = rule.getConditions();
            ConditionRule[] conditionRules = condition.getConditionRule();
            boolean ruleFilled = false;

            //if ((conditionRules.length == 0) || conditionRules == null) ruleFilled = true;

            for (ConditionRule cRule : conditionRules) {
                Field field;

                if (cRule.getOperator().equals("isChecked") || cRule.getOperator().equals("isNotChecked")) {
                    field = fieldFunction.getFieldById(cRule.getName(), fields);
                } else {
                    field = fieldFunction.getFieldByName(cRule.getName(), fields);
                }

                int elementId = elementName.get(field);

                String valS = checkElementConditionValue(field, elementId);
                if (valS.isEmpty() || valS.equals("")) {
                    if (cRule.getOperator().equals("isBlank")) {
                        ruleFilled = true;
                    } else {
                        ruleFilled = false;
                    }
                } else {
                    boolean checking = false;
                    if (cRule.getOperator().equals("isPresent")) {
                        checking = true;
                    } else if (cRule.getOperator().equals("isChecked")) {
                        if (valS.equals("T")) checking = true;
                    } else if (cRule.getOperator().equals("isNotChecked")) {
                        if (!(valS.equals("T"))) checking = true;
                    } else if (cRule.getOperator().equals("isIn")) {
                        if (checkElementConditionValue(field, elementId).contains(cRule.getValue())) {
                            checking = true;
                        }
                    } else if (cRule.getOperator().equals("isNotIn")) {
                        if (!(checkElementConditionValue(field, elementId).contains(cRule.getValue()))) {
                            checking = true;
                        }
                    } else if (cRule.getOperator().equals("equalTo")) {
                        try {
                            if (checkElementConditionValue(field, elementId).equals(cRule.getValue())) {
                                checking = true;
                            }
                        } catch (Exception e) {
                            int val = Integer.parseInt(checkElementConditionValue(field, elementId));
                            if (val == Integer.parseInt(cRule.getValue())) {
                                checking = true;
                            }
                        }
                    } else if (cRule.getOperator().equals("notEqualTo")) {
                        try {
                            if (!(checkElementConditionValue(field, elementId).equals(cRule.getValue()))) {
                                checking = true;
                            }
                        } catch (Exception e) {
                            int val = Integer.parseInt(checkElementConditionValue(field, elementId));
                            if (val == Integer.parseInt(cRule.getValue())) {
                                checking = true;
                            }
                        }
                    } else if (cRule.getOperator().equals("greaterThan")) {
                        int val = Integer.parseInt(checkElementConditionValue(field, elementId));
                        if (val > Integer.parseInt(cRule.getValue())) {
                            checking = true;
                        }
                    } else if (cRule.getOperator().equals("greaterThanEqual")) {
                        int val = Integer.parseInt(checkElementConditionValue(field, elementId));
                        if (val >= Integer.parseInt(cRule.getValue())) {
                            checking = true;
                        }
                    } else if (cRule.getOperator().equals("lessThanEqual")) {
                        int val = Integer.parseInt(checkElementConditionValue(field, elementId));
                        if (val <= Integer.parseInt(cRule.getValue())) {
                            checking = true;
                        }
                    } else if (cRule.getOperator().equals("lessThan")) {
                        int val = Integer.parseInt(checkElementConditionValue(field, elementId));
                        if (val < Integer.parseInt(cRule.getValue())) {
                            checking = true;
                        }
                    }

                    checking = !checking;

                    if (checking) {
                        //Toast.makeText(OpenFormActivity.this, "Ternyata tidak sama", Toast.LENGTH_SHORT).show();
                        if (condition.getType().equals("all")) {
                            ruleFilled = false;
                            break;
                        } else if (condition.getType().equals("any")) {
                            ruleFilled = ruleFilled || false;
                        } else if (condition.getType().equals("none")) {
                            ruleFilled = true;
                        }
                    } else {
                        if (condition.getType().equals("all")) {
                            ruleFilled = true;
                        } else if (condition.getType().equals("any")) {
                            ruleFilled = ruleFilled || true;
                        } else if (condition.getType().equals("none")) {
                            ruleFilled = false;
                            break;
                        }
                    }
                }
            }

            if (conditionRules.length == 0) {
                ruleFilled = true;
                //    Toast.makeText(OpenFormActivity.this, "ada rule kosong", Toast.LENGTH_SHORT).show();
            }

            Action[] actions = rule.getActions();
            //Toast.makeText(OpenFormActivity.this, "rule get", Toast.LENGTH_SHORT).show();
            for (Action act : actions) {
                if (ruleFilled) {
                    setActionRule(act, fields);
                } else {
                    initiateActionsRule(act, fields);
                }
            }
        }

    }

    private List<OpenCondition> formCondition = new ArrayList<>();

    private void setTextWatcher(Field target, int id) {
        if (target.getTagName().equals("textarea")) {
            EditText editText = (EditText) findViewById(id);
            editText.addTextChangedListener(new RuleWatcher(editText));
        } else if (target.getTagName().equals("input")) {
            if (target.getType().equals("radio")) {
                RadioGroup radioGroup = (RadioGroup) findViewById(id);
                radioGroup.setOnCheckedChangeListener(new RadioSelectWatcher(radioGroup));
                //Toast.makeText(OpenFormActivity.this, "rule on rdio", Toast.LENGTH_SHORT).show();
            } else if (target.getType().equals("checkbox")) {
                CheckBox checkBox = (CheckBox) findViewById(id);
                checkBox.setOnCheckedChangeListener(new CheckBoxWatcher(checkBox));
            } else {
                EditText editText = (EditText) findViewById(id);
                editText.addTextChangedListener(new RuleWatcher(editText));
            }
        } else if (target.getTagName().equals("select")) {
            Spinner spinner = (Spinner) findViewById(id);
            spinner.setOnItemSelectedListener(new RuleSelectWatcher(spinner));
            //Toast.makeText(OpenFormActivity.this, "rule on spinner", Toast.LENGTH_SHORT).show();
        }

    }

    public void addTextWatcher(EditText input) {
        input.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mOnStartTimeStamp == 0) {
                    String value = charSequence.toString().trim();
                    if(value.length() > 0){
                        mOnStartTimeStamp = System.currentTimeMillis();
                        Log.d(TAG, "Timer Start: " + mOnStartTimeStamp);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }

        });
    }

    private List<Field> getElementList(ConditionRule rule, Field[] fields) {
        List<Field> fieldList = new ArrayList<Field>();
        if (rule.getOperator().equals("isChecked") || rule.getOperator().equals("isNotChecked")) {
            Field field = fieldFunction.getFieldById(rule.getName(), fields);
            fieldList.add(field);
        } else {
            Field field = fieldFunction.getFieldByName(rule.getName(), fields);
            fieldList.add(field);
        }
        return fieldList;
    }


    private void pushRulesElement(List<Field> fieldList, Rule rule, int x) {
        for (Field field : fieldList) {

            if (elementName.containsKey(field)) {
                int elementId = elementName.get(field);

                //    Toast.makeText(OpenFormActivity.this, Integer.toString(elementId), Toast.LENGTH_SHORT).show();

                OpenCondition openCondition = fieldFunction.getConditionById(formCondition, elementId);
                if (null == openCondition) {
                    setTextWatcher(field, elementId);
                    OpenCondition newCondition = new OpenCondition();
                    newCondition.setId(elementId);
                    newCondition.pushRules(rule);
                    formCondition.add(newCondition);
                } else {
                    openCondition.pushRules(rule);
                }
            }
        }
    }

    private void setRules() {
        Field[] fields = openForm.getFields();
        Rule[] rules = openForm.getRules();
        //Toast.makeText(OpenFormActivity.this, Integer.toString(rules.length), Toast.LENGTH_SHORT).show();

        for (int n = 0; n < rules.length; n++) {

            Condition condition = rules[n].getConditions();
            Action[] actions = rules[n].getActions();
            ConditionRule[] conditionRule = condition.getConditionRule();

            for (ConditionRule cRule : conditionRule) {
                List<Field> fieldList = new ArrayList<Field>();
                fieldList = getElementList(cRule, fields);
                pushRulesElement(fieldList, rules[n], 0);
                //Toast.makeText(OpenFormActivity.this, Integer.toString(n) + field.getTagName(), Toast.LENGTH_SHORT).show();
            }

            if (conditionRule.length == 0) {
                List<Field> fieldList = new ArrayList<Field>();
                for (Field field : fields) {
                    //    Toast.makeText(OpenFormActivity.this, field.getType(), Toast.LENGTH_SHORT).show();
                    if (field.getType().equals("radio")) {
                        String idRadio = field.getId();
                        int lenId = idRadio.length();
                        char idIdRadio = idRadio.charAt(lenId - 1);
                        if ((idIdRadio == '0')) {
                            if (fieldFunction.nothingInAction(actions, forbidenField, field.getName())) {
                                fieldList.add(field);
                            } else {
                                forbidenField.add(field.getName());
                            }
                        }
                    } else {
                        if (fieldFunction.nothingInAction(actions, forbidenField, field.getName())) {
                            fieldList.add(field);
                        } else {
                            forbidenField.add(field.getName());
                        }
                    }
                }
                //    int lengt = fieldList.size();
                pushRulesElement(fieldList, rules[n], 1);
            }

            //Field target = fieldFunction.getFieldByName(condition, fields);


            //Action[] actions = rules[n].getActions();
            //Toast.makeText(OpenFormActivity.this, Integer.toString(actions.length), Toast.LENGTH_SHORT).show();
            for (int l = 0; l < actions.length; l++) {
                //Toast.makeText(OpenFormActivity.this, "rules initiated", Toast.LENGTH_SHORT).show();
                initiateActionsRule(actions[l], fields);
            }

            for (OpenCondition target : formCondition) {
                //Toast.makeText(OpenFormActivity.this, "rules checked", Toast.LENGTH_SHORT).show();
                checkElementCondition(target);
            }

            //Toast.makeText(OpenFormActivity.this, Integer.toString(n), Toast.LENGTH_SHORT).show();
        }

    }

    private void buildForm() {
        Field[] fields = openForm.getFields();
        bodyForm = (LinearLayout) findViewById(R.id.bodyForm1);

        final Map<String, List<Field>> rotateMap = new HashMap<>();
        boolean randomizing = false;
        int startRandomIndex = 0;
        int endRandomIndex = 0;

        int[] wrapper = {0};
        for (; wrapper[0] < fields.length; wrapper[0]++) {
            final boolean nextIsInbound = wrapper[0] + 1 < fields.length;
            if (fields[wrapper[0]].isReadonly()) {

                if (randomizing == false) {
                    startRandomIndex = wrapper[0];
                    randomizing = true;
                }

                final String groupLabel = fields[wrapper[0]].getGroupLabel();
                List<Field> rotateFields = rotateMap.containsKey(groupLabel) ?
                        rotateMap.get(groupLabel) :
                        new ArrayList<Field>();

                rotateFields.add(fields[wrapper[0]]);
                rotateMap.put(groupLabel, rotateFields);

                if (nextIsInbound
                        && fields[wrapper[0] + 1].isReadonly()) {
                    continue;
                } else {
                    endRandomIndex = wrapper[0];
                    randomizing = false;
                    randomizeFields(fields, rotateMap, startRandomIndex, endRandomIndex);
                    rotateMap.clear();
                }
            }
        }
        wrapper[0] = 0;
        for (; wrapper[0] < fields.length; wrapper[0]++) {
            processFields(fields, wrapper);
        }
        openForm.setFields(fields);
        setRules();
    }

    private void randomizeFields(final Field[] fields,
                                 final Map<String, List<Field>> rotateMap,
                                 final int startRandomIndex,
                                 final int endRandomIndex) {

        int size = 0;
        final List<List<Field>> fieldsOrder = new ArrayList<>();
        for (List<Field> fieldList : rotateMap.values()) {
            fieldsOrder.add(fieldList);
            size += fieldList.size();
        }

        Collections.shuffle(fieldsOrder);

        Field[] newFields = new Field[size];
        int it = 0;
        for (List<Field> fieldList : fieldsOrder) {
            for (Field field : fieldList) {
                newFields[it] = field;
                it++;
            }
        }

        for (int i = startRandomIndex; i <= endRandomIndex; i++)
            fields[i] = newFields[i - startRandomIndex];

    }

    private void randomizeReadonlyForm(final Map<String, List<Field>> rotateMap) {
        final List<List<Field>> fieldsOrder = new ArrayList<>();
        int size = 0;
        for (List<Field> fields : rotateMap.values()) {
            fieldsOrder.add(fields);
            size += fields.size();
        }
        Collections.shuffle(fieldsOrder);

        Field[] fields = new Field[size];
        int it = 0;
        for (List<Field> fieldList : fieldsOrder) {
            for (Field field : fieldList) {
                fields[it] = field;
                it++;
            }
        }

        int[] wrapper = {0};
        for (; wrapper[0] < size; wrapper[0]++) {
            processFields(fields, wrapper);
        }
    }

    private void processFields(Field[] fields, int[] wrapper) {
        if ((fields[wrapper[0]].getTagName()).equals("input")) {
            if ((fields[wrapper[0]].getType()).equals("radio")) {
                if (!(elementName.containsKey(fields[wrapper[0]]))) {
                    String radName = fields[wrapper[0]].getName();
                    TextView label = new TextView(OpenFormActivity.this);
                    label.setText(fields[wrapper[0]].getGroupLabel());
                    if (fields[wrapper[0]].isRequired()) {
                        String newLabel = label.getText() + "*";
                        label.setText(newLabel);
                    }
                    label.setTextSize(20);
                    int labelId = View.generateViewId();
                    label.setId(labelId);
                    elementLabel.put(fields[wrapper[0]], labelId);
                    bodyForm.addView(label);

                    RadioGroup radioGroup = new RadioGroup(OpenFormActivity.this);
                    int radioId = View.generateViewId();
                    radioGroup.setId(radioId);
                    elementName.put(fields[wrapper[0]], radioId);

                    //int c = n;
                    while (fields[wrapper[0]].getName().equals(radName)) {
                        RadioButton radioButton = new RadioButton(OpenFormActivity.this);
                        //radioButton.setId(n);
                        radioButton.setText(fields[wrapper[0]].getLabel());
                        radioButton.setId(View.generateViewId());
                        if (fields[wrapper[0]].isChecked()) radioButton.setChecked(true);
                            /*if (n == c) {
                                radioButton.setChecked(true);
                            }*/
                        radioGroup.addView(radioButton);
                        wrapper[0]++;
                        if (wrapper[0] == fields.length) break;
                    }
                    //RadioButton firstRadioButton = (RadioButton) radioGroup.getChildAt(0);
                    //firstRadioButton.setChecked(true);

                    bodyForm.addView(radioGroup);
                    wrapper[0]--;
                }
            } else if ((fields[wrapper[0]].getType()).equals("checkbox")) {
                if (!(elementName.containsKey(fields[wrapper[0]]))) {
                    TextView label = new TextView(OpenFormActivity.this);
                    label.setText(fields[wrapper[0]].getGroupLabel());
                    if (fields[wrapper[0]].isRequired()) {
                        String newLabel = label.getText() + "*";
                        label.setText(newLabel);
                    }
                    label.setTextSize(20);
                    int labelId = View.generateViewId();
                    label.setId(labelId);
                    elementLabel.put(fields[wrapper[0]], labelId);
                    bodyForm.addView(label);
                    String clName = fields[wrapper[0]].getName();

                    while (fields[wrapper[0]].getName().equals(clName)) {
                        CheckBox checkBox = new CheckBox(OpenFormActivity.this);
                        int chkbxId = View.generateViewId();
                        checkBox.setId(chkbxId);
                        elementName.put(fields[wrapper[0]], chkbxId);
                        checkBox.setText(fields[wrapper[0]].getLabel());
                        bodyForm.addView(checkBox);
                        if (fields[wrapper[0]].isChecked()) checkBox.setChecked(true);
                        wrapper[0]++;
                        if (wrapper[0] == fields.length) break;
                    }
                    wrapper[0]--;
                }
            } else {
                TextView label = new TextView(OpenFormActivity.this);
                label.setText(fields[wrapper[0]].getLabel());
                if (fields[wrapper[0]].isRequired()) {
                    String newLabel = label.getText() + "*";
                    label.setText(newLabel);
                }
                label.setTextSize(20);
                int labelId = View.generateViewId();
                label.setId(labelId);
                elementLabel.put(fields[wrapper[0]], labelId);
                bodyForm.addView(label);

                EditText editText = new EditText(OpenFormActivity.this);
                if (fields[wrapper[0]].getType().equals("email")) {
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                } else if (fields[wrapper[0]].getType().equals("date")) {
                    editText.setInputType(InputType.TYPE_NULL);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        editText.setShowSoftInputOnFocus(false);
                    }

                    editText.setOnFocusChangeListener(new popupDateValue());
                    editText.setOnClickListener(new popupDateValue1());

                    editText.setHint("yyyy-MM-dd");
                } else if (fields[wrapper[0]].getType().equals("number")) {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                } else {
                    editText.setInputType(InputType.TYPE_CLASS_TEXT);
                }
                int inptId = View.generateViewId();
                elementName.put(fields[wrapper[0]], inptId);
                editText.setId(inptId);
                editText.setHint(fields[wrapper[0]].getPlaceholder());
                editText.setText(fields[wrapper[0]].getValue());

                addTextWatcher(editText);
                bodyForm.addView(editText);
            }
        } else if ((fields[wrapper[0]].getTagName()).equals("select")) {
            TextView label = new TextView(OpenFormActivity.this);
            label.setText(fields[wrapper[0]].getLabel());
            if (fields[wrapper[0]].isRequired()) {
                String newLabel = label.getText() + "*";
                label.setText(newLabel);
            }
            label.setTextSize(20);
            int labelId = View.generateViewId();
            label.setId(labelId);
            elementLabel.put(fields[wrapper[0]], labelId);
            bodyForm.addView(label);


            Spinner selectList = new Spinner(OpenFormActivity.this);
            int slcId = View.generateViewId();
            selectList.setId(slcId);
            elementName.put(fields[wrapper[0]], slcId);

            bodyForm.addView(selectList);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(OpenFormActivity.this, R.layout.select_label,
                    fields[wrapper[0]].getOptionLabels());
            adapter.setDropDownViewResource(R.layout.select_label_dropdown);
            selectList.setAdapter(adapter);
            selectList.setSelection(fields[wrapper[0]].getSelectedOption());
        } else if ((fields[wrapper[0]].getTagName()).equals("textarea")) {
            TextView label = new TextView(OpenFormActivity.this);
            label.setText(fields[wrapper[0]].getLabel());
            if (fields[wrapper[0]].isRequired()) {
                String newLabel = label.getText() + "*";
                label.setText(newLabel);
            }
            label.setTextSize(22);
            int labelId = View.generateViewId();
            label.setId(labelId);

            elementLabel.put(fields[wrapper[0]], labelId);
            bodyForm.addView(label);

            EditText editText = new EditText(OpenFormActivity.this);
            editText.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
            editText.setSingleLine(false);
            editText.setGravity(0);
            editText.setMinimumHeight(200);

            int txtarId = View.generateViewId();
            elementName.put(fields[wrapper[0]], txtarId);
            editText.setId(txtarId);
            editText.setHint(fields[wrapper[0]].getPlaceholder());
            editText.setText(fields[wrapper[0]].getValue());

            addTextWatcher(editText);
            bodyForm.addView(editText);
        }
        Space blankSpace = new Space(OpenFormActivity.this);
        blankSpace.setMinimumHeight(15);
        bodyForm.addView(blankSpace);
    }

    private JSONObject getDataSubmit() {
        JSONObject data = new JSONObject();

        Field[] fields = openForm.getFields();
        for (int n = 0; n < fields.length; n++) {
            Field field = fields[n];
            String inputVal = "";
            JSONArray inputValArray = new JSONArray();

            try {
                if (field.getTagName().equals("input")) {
                    if (field.getType().equals("radio")) {
                        RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(field));
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        View selectedRadio = radioGroup.findViewById(selectedId);
                        int ind = radioGroup.indexOfChild(selectedRadio);

                        inputVal = fields[n + ind].getValue();

                        while (fields[n + 1].getType().equals("radio") &&
                                fields[n + 1].getName().equals(field.getName())) {
                            n++;
                            if (n + 1 == fields.length) break;
                        }
                    } else if (field.getType().equals("checkbox")) {
                        while (fields[n].getType().equals("checkbox") &&
                                fields[n].getName().equals(field.getName())) {
                            CheckBox checkBox = (CheckBox) findViewById(elementName.get(fields[n]));
                            if (checkBox.isChecked()) {
                                inputValArray.put(fields[n].getValue());
                            }
                            n++;
                            if (n == fields.length) break;
                        }
                        n--;
                    } else {
                        EditText inputText = (EditText) findViewById(elementName.get(field));
                        inputVal = inputText.getText().toString();
                    }
                } else if (field.getTagName().equals("textarea")) {
                    EditText inputText = (EditText) findViewById(elementName.get(field));
                    inputVal = inputText.getText().toString();
                } else if (field.getTagName().equals("select")) {
                    Spinner select = (Spinner) findViewById(elementName.get(field));
                    String[] selectValues = fields[n].getOptionValues();
                    inputValArray.put(selectValues[select.getSelectedItemPosition()]);

                }

                if ((field.getTagName().equals("select")) || (field.getType().equals("checkbox"))) {
                    data.put(field.getName(), inputValArray);
                } else {
                    data.put(field.getName(), inputVal);
                }


            } catch (JSONException e) {

            }

        }

        //Add Time Spent
        long nowTimeStamp = System.currentTimeMillis();
        Log.d(TAG, "Timer Stop: " + nowTimeStamp);
        long totalDuration = 0;
        long duration = nowTimeStamp - mOnStartTimeStamp;
        long createAt = mOnStartTimeStamp;

        if (surveyId != 0) {
            //Edit
            SavedSurvey savedSurvey = db.getSurvey(surveyId);
            String dataJSON = savedSurvey.getData();
            Log.d(TAG + " Saved Data JSON ", dataJSON);

            try {
                JSONObject jsonObj = new JSONObject(dataJSON);
                int savedDuration = jsonObj.getInt("duration");
                Log.d(TAG + " Saved Duration ", savedDuration + "");

                totalDuration = duration + savedDuration;
                createAt =  jsonObj.getLong("create_at");
                Log.d(TAG + " Saved Create At ", createAt + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            totalDuration = duration;
        }
        Log.d(TAG, "Timer Duration: " + duration);
        Log.d(TAG, "Timer Total Duration: " + totalDuration);

        try {
            //Add duration to local db
            data.put("duration", totalDuration);

            //Add create_at to local db, when add only
            data.put("create_at", createAt);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data;
    }

    public boolean cekEmailText(String email) {
        String[] emailText = email.split("@");
        if (emailText.length != 2) {
            return false;
        } else {
            String[] emailAddress = emailText[1].split("\\.");
            if (emailAddress.length < 2) {
                //Toast.makeText(OpenFormActivity.this,emailText[1], Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    public boolean cekEmptyData() {
        Field[] fields = openForm.getFields();
        boolean dataEmpty = true;
        for (int n = 0; n < fields.length; n++) {
            Field field = fields[n];
            String inputVal = "";
            if (field.getTagName().equals("input")) {
                if (field.getType().equals("radio")) {
                    RadioGroup radioGroup = (RadioGroup) findViewById(elementName.get(field));
                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    if (selectedId >= 0) {
                        View selectedRadio = radioGroup.findViewById(selectedId);
                        int ind = radioGroup.indexOfChild(selectedRadio);

                        inputVal = fields[n + ind].getValue();
                    }

                    while (fields[n + 1].getType().equals("radio") &&
                            fields[n + 1].getName().equals(field.getName())) {
                        n++;
                        if (n + 1 == fields.length) break;
                    }
                    if ((inputVal.isEmpty() || inputVal.equals("")) && field.isRequired()) {
                        TextView label = (TextView) findViewById(elementLabel.get(field));
                        String textLabel = label.getText().toString();
                        label.setFocusable(true);
                        label.setFocusableInTouchMode(true);
                        label.setTextColor(Color.RED);
                        Toast.makeText(OpenFormActivity.this, "field " + textLabel + " can not be empty", Toast.LENGTH_LONG).show();
                        label.requestFocus();
                        return true;
                    }
                } else if (field.getType().equals("checkbox")) {
                    int nCbx = 0;
                    while (fields[n].getType().equals("checkbox") &&
                            fields[n].getName().equals(field.getName())) {
                        nCbx++;
                        CheckBox checkBox = (CheckBox) findViewById(elementName.get(fields[n]));
                        if (checkBox.isChecked()) {
                            inputVal = fields[n].getValue();
                        }
                        n++;
                        if (n == fields.length) break;
                    }
                    if ((inputVal.equals("") || inputVal.isEmpty()) && fields[n].isRequired()) {
                        //CheckBox checkBox = (CheckBox) findViewById(elementName.get(fields[n-nCbx]));
                        TextView label = (TextView) findViewById(elementLabel.get(field));
                        String textLabel = label.getText().toString();
                        label.setTextColor(Color.RED);
                        Toast.makeText(OpenFormActivity.this, "field " + textLabel + " can not be empty", Toast.LENGTH_LONG).show();
                        label.setFocusable(true);
                        label.setFocusableInTouchMode(true);
                        label.requestFocus();
                        return true;
                    }
                    n--;
                } else {
                    EditText inputText = (EditText) findViewById(elementName.get(field));
                    inputVal = inputText.getText().toString();
                    if (inputVal.isEmpty() || inputVal.equals("")) {
                        if (field.isRequired()) {
                            TextView label = (TextView) findViewById(elementLabel.get(field));
                            String textLabel = label.getText().toString();
                            label.setTextColor(Color.RED);
                            Toast.makeText(OpenFormActivity.this, "field " + textLabel + " can not be empty", Toast.LENGTH_LONG).show();
                            inputText.requestFocus();
                            return true;
                        }
                    } else if (field.getType().equals("email")) {
                        if (!(cekEmailText(inputVal))) {
                            TextView label = (TextView) findViewById(elementLabel.get(field));
                            String textLabel = label.getText().toString();
                            label.setTextColor(Color.RED);
                            Toast.makeText(OpenFormActivity.this, "wrong email format in field " + textLabel, Toast.LENGTH_LONG).show();
                            inputText.requestFocus();
                            return true;
                        }
                    }
                }
            } else if (field.getTagName().equals("textarea")) {
                EditText inputText = (EditText) findViewById(elementName.get(field));
                inputVal = inputText.getText().toString();
                if (inputVal.isEmpty() || inputVal.equals("")) {
                    if (field.isRequired()) {
                        TextView label = (TextView) findViewById(elementLabel.get(field));
                        String textLabel = label.getText().toString();
                        label.setTextColor(Color.RED);
                        Toast.makeText(OpenFormActivity.this, "field " + textLabel + " can not be empty", Toast.LENGTH_LONG).show();
                        inputText.requestFocus();
                        return true;
                    }
                }
            } else if (field.getTagName().equals("select")) {
                Spinner select = (Spinner) findViewById(elementName.get(field));
                String[] selectValues = fields[n].getOptionValues();
                inputVal = (selectValues[select.getSelectedItemPosition()]);
                if (inputVal.isEmpty() || inputVal.equals("")) {
                    TextView label = (TextView) findViewById(elementLabel.get(field));
                    String textLabel = label.getText().toString();
                    Toast.makeText(OpenFormActivity.this, "field " + textLabel + " can not be empty", Toast.LENGTH_LONG).show();
                    label.setFocusable(true);
                    label.setFocusableInTouchMode(true);
                    label.setTextColor(Color.RED);
                    label.requestFocus();
                    return true;
                }

            }
            if (!(inputVal.isEmpty() || inputVal.equals(""))) {
                dataEmpty = false;
            }
        }
        if (dataEmpty)
            Toast.makeText(OpenFormActivity.this, "your survey is  still empty", Toast.LENGTH_LONG).show();
        return dataEmpty;
    }

    public void storeSurveyToLocal() {
        JSONObject params = new JSONObject();

        if (!(cekEmptyData())) {
            params = getDataSubmit();
            SavedSurvey survey = new SavedSurvey();
            survey.setIdForm(openForm.getId());
            survey.setData(params.toString());

            if (surveyId != 0) {
                survey.setId(surveyId);
                db.updateSurvey(survey);
                Toast.makeText(OpenFormActivity.this, "survey is edited", Toast.LENGTH_SHORT).show();
            } else {
                User user = userLocalStore.getLoggedInUser();
                db.addSurvey(survey, user.userId);
                Toast.makeText(OpenFormActivity.this, "survey is saved", Toast.LENGTH_SHORT).show();
                restartForm();
            }
        }
    }

    public void restartForm() {
        //Reset Timer
        mOnStartTimeStamp = 0;

        Intent mIntent = getIntent();
        finish();
        startActivity(mIntent);
    }

    public void submitToServer() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JSONObject params = new JSONObject();
        params = getDataSubmit();
//        Toast.makeText(OpenFormActivity.this, params.toString(), Toast.LENGTH_LONG).show();

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, apiAddress.submitForm + openForm.getId(),
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(OpenFormActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OpenFormActivity.this, "No connection available", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                User user = userLocalStore.getLoggedInUser();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", "My useragent");
                String auth = "Bearer " + user.oAuth;
                headers.put("Authorization", auth);
                return headers;
            }

        };
        requestQueue.add(stringRequest);
    }

    public void getDataFromId(String id) {
        SavedForm form = db.getFormByIdLocal(id);
        try {
            JSONObject formJson = new JSONObject(form.getBody());
            //Toast.makeText(OpenFormActivity.this, formJson.toString(), Toast.LENGTH_LONG).show();
            setListForm(formJson);
        } catch (Exception e) {

        }
    }

    public void getDataFromLocal(int id) {
        SavedForm form = db.getForm(id);
        try {
            JSONObject formJson = new JSONObject(form.getBody());
            setListForm(formJson);
        } catch (Exception e) {

        }

    }

    public void getDataFromServer(String id) {

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, apiAddress.openForm + id,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setListForm(response);
                        Toast.makeText(OpenFormActivity.this, "Form updated", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OpenFormActivity.this, "Connection failure", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                User user = userLocalStore.getLoggedInUser();
                String auth = "Bearer " + user.oAuth;
                headers.put("Authorization", auth);
                return headers;
            }

        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(jsonRequest);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.saveButton1):
                storeSurveyToLocal();
                break;
        /*    case (R.id.openformNextButton):
                break;
            case (R.id.openformPrevButton):
                break; */
            default:
        }
    }

    DatePickerDialog datePickerDialog;
    SimpleDateFormat dateFormatter;

    public void setDateTimeElement(final int viewId) {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                EditText dateField = (EditText) findViewById(viewId);

                String filledD = dateField.getText().toString();

                String yearD = Integer.toString(newDate.get(Calendar.YEAR));
                String monthD = Integer.toString(1 + newDate.get(Calendar.MONTH));
                String dateD = Integer.toString(newDate.get(Calendar.DATE));

                dateField.setText(yearD + "-" + monthD + "-" + dateD);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public class popupDateValue1 implements View.OnClickListener {
        public popupDateValue1() {

        }

        @Override
        public void onClick(View v) {
            setDateTimeElement(v.getId());
            datePickerDialog.show();
        }
    }


    public class popupDateValue implements View.OnFocusChangeListener {
        public popupDateValue() {

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            //Toast.makeText(OpenFormActivity.this, "testing on click", Toast.LENGTH_SHORT).show();
            if (hasFocus) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                setDateTimeElement(v.getId());
                datePickerDialog.show();
            }
        }
    }


    public class RuleWatcher implements TextWatcher {

        private View view;

        public RuleWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void afterTextChanged(Editable editable) {
            //Toast.makeText(OpenFormActivity.this, "Id = " + view.getId() + " conten = " + editable.toString(), Toast.LENGTH_SHORT).show();
            OpenCondition targetCondition = fieldFunction.getConditionById(formCondition, view.getId());
            //Toast.makeText(OpenFormActivity.this, "Id = " + view.getId() + " rules = " + Integer.toString(targetCondition.getRules().size()), Toast.LENGTH_SHORT).show();
            checkElementCondition(targetCondition);
        }
    }

    public class RuleSelectWatcher implements AdapterView.OnItemSelectedListener {
        private View view;

        public RuleSelectWatcher(View view) {
            this.view = view;
        }

        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            //Toast.makeText(OpenFormActivity.this, "Id = " + view.getId(), Toast.LENGTH_SHORT).show();
            OpenCondition targetCondition = fieldFunction.getConditionById(formCondition, view.getId());
            checkElementCondition(targetCondition);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
            // your code here
        }
    }

    public class RadioSelectWatcher implements RadioGroup.OnCheckedChangeListener {
        private View view;

        public RadioSelectWatcher(View view) {
            this.view = view;
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            OpenCondition targetCondition = fieldFunction.getConditionById(formCondition, view.getId());
            checkElementCondition(targetCondition);

            if (mOnStartTimeStamp == 0) {
                mOnStartTimeStamp = System.currentTimeMillis();
                Log.d(TAG, "Timer Start: " + mOnStartTimeStamp);
            }
            Log.d(TAG, "On Radio Changed ");
        }
    }

    public class CheckBoxWatcher implements CompoundButton.OnCheckedChangeListener {
        private View view;

        public CheckBoxWatcher(View view) {
            this.view = view;
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            OpenCondition targetCondition = fieldFunction.getConditionById(formCondition, view.getId());
            checkElementCondition(targetCondition);

            if (mOnStartTimeStamp == 0) {
                mOnStartTimeStamp = System.currentTimeMillis();
                Log.d(TAG, "Timer Start: " + mOnStartTimeStamp);
            }
            Log.d(TAG, "On Check Box Changed ");
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new FormSlideFragment();
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}

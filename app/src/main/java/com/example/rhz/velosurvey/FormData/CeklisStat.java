package com.example.rhz.velosurvey.FormData;

/**
 * Created by rhz on 2/19/17.
 */

public class CeklisStat {
    private int stat;

    public CeklisStat() {
        stat = 0;
    }

    public  int getStat() {
        return stat;
    }

    public void setStat(int s) {
        stat = s;
    }

}

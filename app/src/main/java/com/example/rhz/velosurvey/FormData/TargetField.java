package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/28/2016.
 */
public class TargetField {
    public String name;
    public String value;

    public TargetField(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package com.example.rhz.velosurvey.FormData;

/**
 * Created by RHZ on 3/22/2016.
 */
public class Condition {
    private String type;
    private ConditionRule[] conditionRule;

    public Condition() {
    }

    public Condition(String type, ConditionRule[] conditionRule) {
        this.type = type;
        this.conditionRule = conditionRule;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ConditionRule[] getConditionRule() {
        return conditionRule;
    }

    public void setConditionRule(ConditionRule[] conditionRule) {
        this.conditionRule = conditionRule;
    }
}

package com.example.rhz.velosurvey.FormAction;

import com.example.rhz.velosurvey.FormData.Action;
import com.example.rhz.velosurvey.FormData.Field;
import com.example.rhz.velosurvey.FormData.OpenCondition;
import com.example.rhz.velosurvey.FormData.TargetField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RHZ on 4/26/2016.
 */
public class FieldFunction {

    public OpenCondition getConditionById(List<OpenCondition> conditions, int id) {
        for (OpenCondition condition : conditions) {
            if(condition.getId() == id) {
                return condition;
            }
        }

        return null;
    }



    public Field getFieldById(String id, Field[] fields) {

        for(Field itr : fields) {
            if (itr.getId().equals(id)) {
                return itr;
            }
        }
        return null;
    }

    public int getIdByField(Field field, Field[] fields) {

        for(int n = 0; n < fields.length; n++) {
            if(fields[n].getId().equals(field.getId())) {
                return n;
            }
        }

        return -1;
    }

    public Field getFieldByName(String name, Field[] fields) {

        for(Field itr : fields) {
            if (itr.getName().equals(name)) {
                return itr;
            }
        }
        return null;
    }

    public boolean nothingInAction(Action[] actions, List<String> forbidenField,String id) {
        for(Action act : actions) {
            List<TargetField> targetFields = act.getActionFields();
            for (TargetField targ : targetFields) {
                if(targ.getValue().equals(id)) return false;
            }
        }
        for(String fF : forbidenField) {
            if(fF.equals(id)) return false;
        }

        return true;
    }

    public List<Field> getMultiFieldByName(String name, Field[] fields) {
        List<Field> multiField = new ArrayList<Field>();

        for(Field itr : fields) {
            if (itr.getName().equals(name)) {
                multiField.add(itr);
            }
        }

        return multiField;
    }
}

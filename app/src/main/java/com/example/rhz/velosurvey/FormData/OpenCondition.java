package com.example.rhz.velosurvey.FormData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RHZ on 4/27/2016.
 */
public class OpenCondition {
    private int id;
    private List<Rule> rules;

    public OpenCondition() {
        id = 0;
        rules = new ArrayList<Rule>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void pushRules(Rule rule) {
        this.rules.add(rule);
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }
}
